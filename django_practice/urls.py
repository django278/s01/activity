from django.urls import path
from . import views

# path() function
#path (route, view, name)
urlpatterns = [
    path('', views.index, name='index'),
]